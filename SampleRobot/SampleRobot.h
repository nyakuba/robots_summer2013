#pragma once
#include "interfaces\IRobot.h"
#include <queue>

#include "ThreadLock.h"

using namespace RobotWar;

/** 
	@brief ����� ������.
*/
class SampleRobot :
	public IRobot
{
	// ������.
	enum State {
		NOPE = 0,
		MOVE,
		FIRE,
		LOOK,
		LEAD,
		WAIT
	};

	// ��������� �������.
	enum Command {
		CM_NOTHING = 0,
		CM_CLEAR_STATE_QUEUE,
		CM_SET_STATE_QUEUE,
		CM_SET_FIRE_POS,
		CM_SET_MOVE_POS,
		CM_SET_ENEMIES
	};

	struct TalkData{
		TalkData(){ 
			for (int i=0;i < BYTE_SIZE; i++)
				m_data[i] = 0;
			for (int i=0;i < CMS_SIZE; i++)
				m_cms[i] = CM_NOTHING;
			for (int i=0;i < ENEMIES_SIZE; i++)
				m_enemies[i] = Vec2d(0,0);
			for (int i=0;i < STATES_SIZE; i++)
				m_states[i] = NOPE;
		}
		//TalkData(std::vector<Command> &cms, Vec2d &wantToMove, Vec2d &wantToFire, std::vector<Vec2d> &enemies,	std::vector<State> &states ):
		//cms(cms), wantToMove(wantToMove), wantToFire(wantToFire), enemies(enemies), states(states) {}
		static const unsigned CMS_SIZE = 3;
		static const unsigned ENEMIES_SIZE = 2;
		static const unsigned STATES_SIZE = 10;
		static const unsigned BYTE_SIZE = CMS_SIZE+2*2+ENEMIES_SIZE*2+STATES_SIZE;

		Command m_cms[CMS_SIZE];
		Vec2d m_wantToMove;
		Vec2d m_wantToFire;
		Vec2d m_enemies[ENEMIES_SIZE];
		State m_states[STATES_SIZE];
		BYTE m_data[BYTE_SIZE];

		void set( const BYTE *data ){
			for( int i = 0; i < CMS_SIZE; i++){
				switch(data[i]){
					case 0: m_cms[i] = CM_NOTHING; break;
					case 1: m_cms[i] = CM_CLEAR_STATE_QUEUE; break;
					case 2:	m_cms[i] = CM_SET_STATE_QUEUE; break;
					case 3: m_cms[i] = CM_SET_FIRE_POS; break;
					case 4: m_cms[i] = CM_SET_MOVE_POS; break;
					case 5: m_cms[i] = CM_SET_ENEMIES; break;
				}
			}
			m_wantToMove.x = data[CMS_SIZE];
			m_wantToMove.y = data[CMS_SIZE+1];
			m_wantToFire.x = data[CMS_SIZE+2];
			m_wantToFire.y = data[CMS_SIZE+3];

			for( int i = CMS_SIZE+4, enemiesCount = 0; i < CMS_SIZE+4+ENEMIES_SIZE*2; i+=2, enemiesCount++){
				m_enemies[enemiesCount].x = data[i];
				m_enemies[enemiesCount].y = data[i+1];
			}

			for( int i = CMS_SIZE+4+ENEMIES_SIZE*2, j = 0; i < BYTE_SIZE; i++, j++){
				switch(data[i]){ 
				case 0: m_states[j] = NOPE; break;
				case 1: m_states[j] = MOVE; break;
				case 2: m_states[j] = FIRE; break;
				case 3: m_states[j] = LOOK; break;
				case 4: m_states[j] = LEAD; break;
				case 5: m_states[j] = WAIT; break;
				}
			}
		}

		operator BYTE *() {
			for( int i = 0; i < CMS_SIZE; i++){
				m_data[i] = m_cms[i];
			}

			m_data[CMS_SIZE] = m_wantToMove.x;
			m_data[CMS_SIZE+1] = m_wantToMove.y;
			m_data[CMS_SIZE+2] = m_wantToFire.x;
			m_data[CMS_SIZE+3] = m_wantToFire.y;

			for( int i = CMS_SIZE+4, enemiesCount = 0; i < CMS_SIZE+4+ENEMIES_SIZE*2; i+=2, enemiesCount++){
				m_data[i] = m_enemies[enemiesCount].x;
				m_data[i+1] = m_enemies[enemiesCount].y;
			}

			for( int i = CMS_SIZE+4+ENEMIES_SIZE*2, j = 0; i < BYTE_SIZE; i++, j++){
				m_data[i] = m_states[j];
			}

			return m_data;
		}
	};

	string m_name;
	State m_state;
	bool ImLead;
	/// Utils.
	Vec2d nextStep(Vec2d relativePos);

public:
	SampleRobot(void);
	~SampleRobot(void);

	ThreadLock lock;	

	/// ���������. ���������������� � ������ ���.
	unsigned m_teamId;
	int MAX_HEALTH;
	int MAX_LOOK_DEPTH;
	int FIRE_RANGE;
	Vec2d AREA_SIZE;

	/// ������ �� IRobotOperation
	IRobotOperation* m_engine;

	/// ������� ��������.
	std::queue<State> m_actionQueue;
	std::queue<Command> m_commandQueue;

	/// �������, �������� ������ �������.
	std::vector<RobotInfo> m_foundRobots;
	std::vector<RobotInfo> m_enemies;
	std::vector<RobotInfo> m_teamRobots;

	/// ������� �������: ������, ��������, ��������.
	Vec2d m_currentPosition; // ����������
	Vec2d m_firePos; // �������������
	Vec2d m_movePos; // ����������
	string m_msgSender;

	/// @return ��� ������.
	string getName(void);

	/// @brief �����, ���������� ��� ��������� ���������.
	/// @param RobotName - ��� �����������.
	/// @param msg - ������ ������.
	/// @param msgSize - ������ ������� msg.
	void ReceiveMessage(const string &RobotName, const BYTE *msg, unsigned msgSize);

	/// @brief �������� ����� ���������� ��������� ������.
	/// @param Engine - ������ �� ���������, ���������� ��� ��������� ��� ���������� ������� ��������.
	void Execute(IRobotOperation *Engine);

	void performLEAD( void );
	void performFIRE( void );
	void performLOOK( void );
	void performMOVE( void );
};
