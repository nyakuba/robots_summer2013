#pragma once

#include "ThreadLock.h"


ThreadLock::ThreadLock(void)
	: m_nReaderCount(0), m_hWriterEvent(NULL), m_hNoReadersEvent(NULL)
{
	// Create writer event with manual reset and default signaled state.
	// 

	// State:

	//          Signaled     = Writer has currently not access.
	//          Non-signaled = Writer has currently access, block readers.
	//

	m_hWriterEvent    = CreateEvent(NULL, TRUE, TRUE, NULL);

	// Create no readers event with manual reset and default signaled state.
	// 

	// State:

	//          Signaled     = No readers have currently access.
	//          Non-signaled = Some readers have currently access, block writer.
	//

	m_hNoReadersEvent = CreateEvent(NULL, TRUE, TRUE, NULL);

	//
	// Initialize critical sections.

	InitializeCriticalSection(&m_csLockWriter);
	InitializeCriticalSection(&m_csReaderCount);
}

ThreadLock::ThreadLock(const ThreadLock &cReadWriteLock)
{
}

const ThreadLock& ThreadLock::operator=(const ThreadLock &cReadWriteLock)
{
	return *this;
}


ThreadLock::~ThreadLock(void)
{
	//
	// Delete critical sections.

	DeleteCriticalSection(&m_csLockWriter);
	DeleteCriticalSection(&m_csReaderCount);

	// Close the writer event.

	CloseHandle(m_hWriterEvent);
	// Close the no readers event.

	CloseHandle(m_hNoReadersEvent);
}


void ThreadLock::LockReader()
{
	bool bLoop = true;

	// Loop.

	while(bLoop)
	{
		// Wait for Writer event to be signaled.

		WaitForSingleObject(m_hWriterEvent, INFINITE);
		// Increment number of readers.

		IncrementReaderCount();
		// If writer is become non-signaled fall back (double locking).

		if(WaitForSingleObject(m_hWriterEvent, 0) != WAIT_OBJECT_0)
		{
			// Decrement number of readers.
			DecrementReaderCount();
		}
		else
		{
			// Breakout.

			bLoop = false;
		}
	}
}


void ThreadLock::UnlockReader()
{
	// Decrement number of readers.

	DecrementReaderCount();
}


void ThreadLock::LockWriter()
{
	// Enter critical section (prevent more than one writer).

	EnterCriticalSection(&m_csLockWriter);
	// Wait for current writer.

	WaitForSingleObject(m_hWriterEvent, INFINITE);
	// Set writer to non-signaled.

	ResetEvent(m_hWriterEvent);
	// Wait for current readers to finish.

	WaitForSingleObject(m_hNoReadersEvent, INFINITE); 
	// Leave critical section.

	LeaveCriticalSection(&m_csLockWriter);
}


void ThreadLock::UnlockWriter()
{
	// Set writer event to signaled.

	SetEvent(m_hWriterEvent);
}


void ThreadLock::IncrementReaderCount()
{
	// Enter critical section.

	EnterCriticalSection(&m_csReaderCount);
	// Increase reader count.

	m_nReaderCount++;
	// Reset the no readers event.

	ResetEvent(m_hNoReadersEvent);
	// Leave critical section.

	LeaveCriticalSection(&m_csReaderCount);
}


void ThreadLock::DecrementReaderCount()
{
	// Enter critical section.

	EnterCriticalSection(&m_csReaderCount);
	// Decrease reader count.

	m_nReaderCount--;
	// Are all readers out?

	if(m_nReaderCount <= 0)
	{
		// Set the no readers event.

		SetEvent(m_hNoReadersEvent);
	}
	// Leave critical section.

	LeaveCriticalSection(&m_csReaderCount);
}