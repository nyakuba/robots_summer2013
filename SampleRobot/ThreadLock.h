#pragma once

/* Looking for a simple solution on the net, I faced the fact that the solutions often were advanced, 
	and not developed in the spirit of KISS (Keep It Simple S*****). Therefore, I decided to implement a simple yet functional solution.

	Please note that in order to prevent reader starvation, the CReadWriteLock should only be used 
	when reader access is far more common than writer access. The reason for this is 
	that the implementation does not contain any thread queue or similar to secure the access order, 
	and writer threads will always win this race. */


#include "Windows.h"

class ThreadLock {
private:
public:
	//! Constructor.

	ThreadLock(void);
	//! Destructor.

	virtual ~ThreadLock(void);

	//! Lock for reader access.

	void LockReader();
	//! Unlock reader access.

	void UnlockReader();

	//! Lock for writer access.

	void LockWriter();
	//! Unlock writer access.

	void UnlockWriter();

private:
	//! Private copy constructor.

	ThreadLock(const ThreadLock &cReadWriteLock);
	//! Private assignment operator.

	const ThreadLock& operator=(const ThreadLock &cReadWriteLock);

	//! Increment number of readers.

	void IncrementReaderCount();
	//! Decrement number of readers.

	void DecrementReaderCount();

	//! Writer access event.

	HANDLE m_hWriterEvent;
	//! No readers event.

	HANDLE m_hNoReadersEvent;
	//! Number of readers.

	int m_nReaderCount;

	//! Critical section for protecting lock writer method.

	CRITICAL_SECTION m_csLockWriter;
	//! Critical section for protecting reader count.

	CRITICAL_SECTION m_csReaderCount;
};