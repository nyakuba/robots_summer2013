#pragma once

#include "commondf.h"

namespace RobotWar
{
	class Vec2d
	{
	public:
		int x, y;

		Vec2d( void ) : x(0), y(0)
		{
		}

		Vec2d( int x, int y ) : x(x), y(y)
		{
		}

		Vec2d & set( int x, int y )
		{
			this->x = x;
			this->y = y;

			return *this;
		}

		Vec2d operator +( const Vec2d &v ) const
		{
			return Vec2d(x + v.x, y + v.y);
		}

		Vec2d & operator +=( const Vec2d &v )
		{
			x += v.x;
			y += v.y;

			return *this;
		}

		Vec2d operator -( const Vec2d &v ) const
		{
			return Vec2d(x - v.x, y - v.y);
		}

		Vec2d & operator -=( const Vec2d &v )
		{
			x -= v.x;
			y -= v.y;

			return *this;
		}

		bool operator ==( const Vec2d &v ) const
		{
			return (x == v.x) && (y == v.y);
		}

		bool operator !=( const Vec2d &v ) const 
		{
			return (x != v.x) || (y != v.y);
		}

		Vec2d & vecModuling( const Vec2d &area )
		{
			x %= area.x;
			y %= area.y;

			if (x < 0)
				x += area.x;
			if (y < 0)
				y += area.y;

			return *this;
		}

		/// @brief ���������� ������������� ����������.
		/// a.getRelative(b, AREA_SIZE) �������� ���������� b ������������ a
		/// � ������ ������ �� ������� ����.
		/// @param v - ����������, ��������� ������� �����������.
		/// @param area - ������ ����.
		Vec2d getRelative( const Vec2d &v, const Vec2d &area ) const {
			// ������� ���������� � ������ ������ �� ������� ����� � ���� ������� � � ������.
			Vec2d dist1 = v - *this;
			Vec2d dist2 = (v - area) - *this;
			Vec2d dist3 = (v + area) - *this;
			// ��������� ��������� � ����������� ������������
			// �������� �� ������ ����������.
			Vec2d result;
			result.x = ( abs(dist1.x) < abs(dist2.x) ? dist1.x : dist2.x );
			if( abs(result.x) > abs(dist3.x) ){
				result.x = dist3.x;
			}
			result.y = ( abs(dist1.y) < abs(dist2.y) ? dist1.y : dist2.y );
			if( abs(result.y) > abs(dist3.y) ){
				result.y = dist3.y;
			}
			// ��� ��������� ������ �����������, ������ ������� �� �����.
			return result;
		}

		operator COORD( void ) const
		{
			COORD c = {x, y};
			return c;
		}
	};
}