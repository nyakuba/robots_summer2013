#include "SampleRobot.h"
#include "interfaces\IRobotOperation.h"
#include "commondf.h"
#include <sstream>

using namespace std;

/// ������������.
SampleRobot::SampleRobot(void)
{
	m_name = "k580never ";
	m_state = LOOK;
	ImLead = 0;
}

SampleRobot::~SampleRobot(void)
{
}



/// ��������� �����.
string SampleRobot::getName(void)
{
	return m_name;
}

/// ����� ���������.
void SampleRobot::ReceiveMessage(const string &RobotName, const BYTE *msg, unsigned msgSize)
{
	TalkData com;
	com.set(msg);

	for( int i = 0; i < com.CMS_SIZE; i++ ){
		switch(com.m_cms[i]){
		case CM_NOTHING: break;
		case CM_SET_MOVE_POS:
			// �� ������������.
			m_movePos = com.m_wantToMove;
			m_movePos.vecModuling(AREA_SIZE);
			break;
		case CM_SET_ENEMIES:
			lock.LockWriter();
			for( int i=0;i < com.ENEMIES_SIZE; i++){
				if(com.m_enemies[i] != Vec2d(0,0)){
					m_enemies.push_back(RobotInfo("",com.m_enemies[i],100,m_teamId+1));
				}
				if(!ImLead){
					m_firePos = com.m_enemies[0];
					m_state = FIRE;
				}
			}
			m_msgSender = RobotName;
			m_firePos = com.m_enemies[0];
			m_state = FIRE;
			lock.UnlockWriter();
		}
	}
}

/// �������� �����.
void SampleRobot::Execute(IRobotOperation *Engine)
{
	// �������������.
	srand(GetCurrentThreadId());
	m_engine = Engine;

	m_engine->getTeamInfo(this, m_teamRobots);
	m_teamId = m_teamRobots[0].teamId;
	MAX_HEALTH = m_teamRobots[0].health;
	MAX_LOOK_DEPTH = m_engine->getMaxLookDepth();
	FIRE_RANGE = m_engine->getFireRange();
	AREA_SIZE = m_engine->getAreaSize();

	m_movePos = (m_engine->getPosition(this)).vecModuling(AREA_SIZE);

	m_firePos = Vec2d(0,0);

	// �������� ����.
	while ( true ){
		m_currentPosition = m_engine->getPosition(this);
		
		m_engine->getTeamInfo(this, m_teamRobots);
		if(!ImLead)
			if(m_teamRobots[0].pos == m_currentPosition){
				ImLead = true;
			}
		if(!ImLead){
			Vec2d rel = (m_teamRobots[0].pos - m_currentPosition);
			rel.x %= AREA_SIZE.x;
			rel.y %= AREA_SIZE.y;
			if( abs(rel.x) > 7 || abs(rel.y) > 7 ){
				m_movePos = m_teamRobots[0].pos;
				m_state = MOVE;
			}
		}
		if(ImLead)
			performLEAD();

		switch (m_state){
		case FIRE:
			performFIRE();
			break;
		case LOOK:
			performLOOK();
			break;
		case MOVE:
			performMOVE();
			break;
		case WAIT:
			// does nothing
			break;
		}

	}
}

/// ������ ����������.
void SampleRobot::performLOOK( void ){	
	// ������� �� ��������, ���� ��������� ����, ���� ��� �������.
	// ����� �������� � �������� ��������� �����������.
	m_engine->LookAround(this,MAX_LOOK_DEPTH,m_foundRobots);

	lock.LockWriter();
	m_enemies.clear();
	// ����������, ������ �� ����.
	for( auto &robot: m_foundRobots ){
		if( robot.teamId != m_teamId ){ 
			m_enemies.push_back(robot);
		}
	}
	lock.UnlockWriter();
	lock.LockReader();
	if( !m_enemies.empty() ){
		// �������� ����� ��� ����� � ���� attack_rate.
		// attack_rate = 0.
		// attack_rate += 50, ���� ���� � ���� ������������.
		// attack_rate += MAX_HEALTH - enemy health.
		// attack_rate -= 10*dist.
		int max_attack_rate = INT_MIN;
		RobotInfo* target = nullptr;

		for( auto &enemy : m_enemies ){
			Vec2d dist = enemy.pos; // ������� �������������.
			int attack_rate = 0;
			attack_rate += ((dist.x > FIRE_RANGE || dist.y > FIRE_RANGE) ? 0 : 10*MAX_LOOK_DEPTH);
			attack_rate -= MAX_HEALTH - enemy.health;
			attack_rate -= 10*(dist.x + dist.y)/2;
			if( attack_rate > max_attack_rate ){
				max_attack_rate = attack_rate;
				target = &enemy;
			}
		}
		lock.UnlockReader();

		// �������� � ������ ���������.
		if(!ImLead){
			TalkData talkData;
			talkData.m_cms[0] = CM_SET_ENEMIES;
			for( unsigned i = 0; i < min(m_enemies.size(),talkData.ENEMIES_SIZE); i++)
				talkData.m_enemies[i] = m_enemies[i].pos;

			try {
				m_engine->SendMsg(this, m_teamRobots[0].name, talkData, talkData.BYTE_SIZE);
			} catch (RobotWar::exc::RobotWarException){}
		}

		// ��������.
		m_firePos = target->pos;
		m_state = FIRE;
		
	} else { 
		lock.UnlockReader();
		// ���� ����� �� �������, ��������� � ��������� �����, ���� �������� ����� ����������. 
		// ����� ���������� �������� � �������� �����.
		// � �������� ���� ����� �������� ������� ������ ������.
		if ( !ImLead ){
			Vec2d leadPos = m_teamRobots[0].pos;

			if(m_teamRobots[1].pos == m_currentPosition)
				m_movePos = leadPos + Vec2d(-2, 0);
			else if(m_teamRobots[2].pos == m_currentPosition)
					m_movePos = leadPos + Vec2d(0, -2);
			else if(m_teamRobots[3].pos == m_currentPosition)
					m_movePos = leadPos + Vec2d(2, 0);
			else if(m_teamRobots[4].pos == m_currentPosition)
					m_movePos = leadPos + Vec2d(0, 2);
			m_movePos.vecModuling(AREA_SIZE);
		} else if(m_movePos == m_currentPosition)
			m_movePos = Vec2d(rand(), rand()).vecModuling(AREA_SIZE);

		m_state = MOVE;
	}
}

/// ��������.
void SampleRobot::performFIRE( void ){
	// ��������, ���� ��������� � ���� ������������.
	// ���� � ����, ���� ���.
	if(abs(m_firePos.x) <= FIRE_RANGE && abs(m_firePos.y) <= FIRE_RANGE){
		try{
			m_engine->Fire(this,m_firePos); // ���� ��� ��� ����?
			m_engine->Fire(this,m_firePos);		
		} catch (RobotWar::exc::RobotWarException){}
		m_state = LOOK;
	} else {
		m_movePos = (m_firePos + m_currentPosition).vecModuling(AREA_SIZE);
		m_state = MOVE;
	}
}

/// ��������.
void SampleRobot::performMOVE( void ){
	// TODO: �������! ��� ������� ���������� �������� ��������.
	// ���� � ���, ��� ���� �������� ����������, ��������� ������� ���������� ��������.
	bool success = true;
	try{
	// ��� �� �������?
		if ( m_currentPosition == m_movePos ){
			m_state = LOOK;
			return;
		}
	// ���� ������ ����� - �������� � ���.
	// ����� ���������� m_foundRobots.
	// ���� x �������� ����� > 0, �������� �������� ������ � ���. ���������� � x = 0 | 1.
	// ���� x �������� ����� < 0, �������� �������� ������ � ���. ���������� � x = 0 | -1.
	// ��� y ����������.
	// => ��������� 3 ������ (������ ��� x>0, y<0): (0, -1), (1, -1), (1, 0).
	// ���� ���� �� � ����� �� ��� ������ ��� - �������� ����.
		Vec2d relativePos = m_currentPosition.getRelative(m_movePos, AREA_SIZE - Vec2d(1,1));

		// TODO!!! ����� �������. relativePos ����� ��������� (0,0)???
		if(relativePos == Vec2d(0,0)){
			m_movePos = Vec2d(rand(),rand()).vecModuling(AREA_SIZE);
			Vec2d relativePos = m_currentPosition.getRelative(m_movePos, AREA_SIZE - Vec2d(1,1));
		}

		if ( abs(relativePos.x) <= 1 && abs(relativePos.y) <= 1 ){
				m_engine->Move(this, relativePos);
			m_state = LOOK;
		} else {
			// ������� ���������.
			Vec2d nextPos = nextStep(relativePos);		
			if( nextPos != Vec2d(0,0) ){

					m_engine->Move(this, nextPos);
			}
		}
		m_state = LOOK;
	} catch(RobotWar::exc::RobotWarException){
		success = false;
	}
	if(!success){
		try{
			m_engine->Move(this, Vec2d(rand()%3-1,rand()%3-1));
		} catch (RobotWar::exc::RobotWarException){}
	}
}

/// ������� �������� ������.
void SampleRobot::performLEAD( void ){
	// ���� ������� ����, ����� ����, �������� � ��� ��������� �������.
	lock.LockWriter();
	if( !m_enemies.empty() ){
		m_movePos = m_firePos = m_enemies[0].pos;
		m_state = FIRE;

		m_enemies.clear();
		m_msgSender = "";
		lock.UnlockWriter();

		TalkData talkData;
		talkData.m_cms[0] = CM_SET_ENEMIES;
		talkData.m_enemies[0] = m_firePos;
		try{
			for( auto &i : m_teamRobots){
				if(i.name == m_msgSender)
					continue;
				//m_engine->SendMsg(this, i.name, talkData, talkData.BYTE_SIZE);
			}
		} catch (RobotWar::exc::RobotWarException){}
	}
	lock.UnlockWriter();
}

//////////////////////////////////////////////////////////////////////////
/// Utils
//////////////////////////////////////////////////////////////////////////
/// ���������� ���������� ���� �� ����������� � ����.
Vec2d SampleRobot::nextStep(Vec2d relativePos){
	// TODO: ���������� �����!!!
	Vec2d posibilities[4];
	if( relativePos.x > 0 ){
		posibilities[0].x = 1;
		posibilities[1].x = 1;
		posibilities[2].x = 0;
		posibilities[3].x = -1;
	} else if(relativePos.x == 0){
		posibilities[0].x = 0;
		posibilities[1].x = -1;
		posibilities[2].x = 1;
		posibilities[3].x = 0;
	} else {
		posibilities[0].x = -1;
		posibilities[1].x = -1;
		posibilities[2].x = 0;
		posibilities[3].x = 1;
	}
	// ��� y.
	if( relativePos.y > 0 ){
		posibilities[0].y = 1;
		posibilities[1].y = 1;
		posibilities[2].y = 0;
		posibilities[3].y = -1;
	} else if(relativePos.y == 0){
		posibilities[0].y = 0;
		posibilities[1].y = 1;
		posibilities[2].y = -1;
		posibilities[3].y = 0;
	} else {
		posibilities[0].y = -1;
		posibilities[1].y = -1;
		posibilities[2].y = 0;
		posibilities[3].y = 1;
	}
	// �������, �� ������ �� ��� ������.
	// ���� ��� ������ - �������� �� �����.
	Vec2d nextPos;
	bool found = true;
	for( const auto &position : posibilities ){
		found = true;
		for( const auto &robot : m_foundRobots ){
			if( m_currentPosition.getRelative(robot.pos, AREA_SIZE) == position ){
				found = false;
				break;
			}
		}
		if(found){
			nextPos = position;
			break;
		}
	}
	return nextPos;
}