#pragma once

#include "..\commondf.h"

using std::string;

namespace RobotWar
{
	struct RobotInfo {
		RobotInfo(const string &name, const Vec2d &pos, const int &health, const unsigned &teamId) : name(name), pos(pos), health(health), teamId(teamId) {}
		string name;
		Vec2d pos;
		int health;
		unsigned teamId;
	};

	class IRobotOperation;
	class RobotInternal;
	/**
	*	@brief	��������� IRobot ����������� ������� ������������, ������� ������ ���� ���������� �� ������� ������������ �����������, ����������� �� ���������� ������������
	*/
	class IRobot
	{
		
	public:
		string name;
		static const int VERSION = 5;

		//virtual ~IRobot( void ) {}

		/// ����� ��������. ���������� ����� ���������, ����� ������ ����� �������� ��������� ������, ������������ ������ ����� ����������
		virtual void ReceiveMessage( const string& RobotName, const BYTE *msg, unsigned msgSize ) = 0;

		/// ����� ���������� ������ ������. ���� ����� ���������� ��� �������� ��� ������ ���������� ������
		virtual void Execute( IRobotOperation *Engine ) = 0;

		/// @return ��� ������.
		virtual string getName( void ) = 0;

		friend class RobotInternal;
	};
}