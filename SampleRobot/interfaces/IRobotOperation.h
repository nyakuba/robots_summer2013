#pragma once

#include "IRobot.h"
#include "../commondf.h"

namespace RobotWar
{
	/**
		@brief ��������� ������.
	*/
	class IRobotOperation
	{
	public:

		/// ��������� ����������� ��������:
		/// @return ������ ����.
		virtual Vec2d getAreaSize() = 0;
		/// @return ������ ��������.
		virtual int getFireRange() = 0;
		/// @return ������ ������.
		virtual int getMaxLookDepth() = 0;

		/// ����� ��������� ������� ���������� ���������� ������
		/// @param Sender - �����, ������� ���������� � ����. ������� ��������� this
		virtual Vec2d getPosition( IRobot *Sender ) = 0;

		/// ���������� ������ �� ��������� ���������� ��������� (�� ����� 3)
		/// @param Sender - �����, ������� ���������� � ����. ������� ��������� this
		/// @param x - ���������� �� x (0<=x<3)
		/// @param y - ���������� �� y (0<=y<3)
		virtual void Move( IRobot *Sender, const Vec2d &coord ) = 0;

		/// ��������� ������ ���������� ������� �� ��������� ���������� (�� ����� 3)
		/// @param Sender - �����, ������� ���������� � ����. ������� ��������� this
		/// @param x - ���������� �� x (0<=x<3)
		/// @param y - ���������� �� y (0<=y<3)
		virtual void Fire( IRobot *Sender, const Vec2d &coord ) = 0;

		///��������� ������ �������� ���������� � ������������� ������ ������� � ������� depth
		/// @param Sender - �����, ������� ���������� � ����. ������� ��������� this
		/// @param depth - ������� ��������� (0<=depth<3)
		/// @param CountPoints - ���������� ��������� � ������ ��� ���������� ������� ���������
		/// @param pPoints - ������ �������� PT ��� ���������� ���������, � ������� ���� �������
		virtual void LookAround( IRobot *Sender, unsigned depth, std::vector<RobotInfo> &points ) = 0;

		virtual void getTeamInfo( IRobot *Sender, std::vector<RobotInfo> &teamMembers ) = 0;

		///��������� ������ ��������� ��������� ������� ������
		/// @param Sender - �����, ������� ���������� � ����. ������� ��������� this
		/// @param RobotName - ������, ���������� ��� ������, �������� ������������ ���������
		/// @param TalkData - �����, ���������� ������������ ������� ������ ������
		virtual void SendMsg( IRobot *Sender, const string &ReceiverName, BYTE *msg, unsigned msgSize ) = 0;
	};
}