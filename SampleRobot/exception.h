#pragma once

#include "commondf.h"

namespace RobotWar
{
	namespace exc
	{
		/* RobotWarException class */
		class RobotWarException
		{
		private:
			const char *str_;
			bool doFree_;

			/* Free memory if it need internal function
			 * ARGUMENTS: None.
			 * RETURNS: None.
			 */
			inline void _freeMem( void )
			{
				if (doFree_ == true)
					delete[] str_;
				str_ = nullptr;
				doFree_ = false;
			} /* End of 'freeMem' function */

			/* Copy string to class function
			 *   assumes that 'str_' is already free
			 * ARGUMENTS:
			 *   - string to copy
			 *       const char *str;
			 * RETURNS: None.
			 */
			inline void _copyStr( const char *str )
			{
				if (str != nullptr)
				{
					size_t len = strlen(str) + 1;
					str_ = new char[len];
					if (str_ != nullptr)
					{
						memcpy(const_cast<char *>(str_), str, len);
						doFree_ = true;
					}
				}
			} /* End of 'copyStr' function */

		public:
			/* 'RobotWarException' class constructor
			 * ARGUMENTS: None.
			 */
			inline RobotWarException( void ) : str_(nullptr), doFree_(false)
			{
			} /* End of 'RobotWarException' class constructor */

			/* 'RobotWarException' class constructor
			 * ARGUMENTS:
			 *   - pointer to string
			 *       const char *str;
			 */
			inline RobotWarException( const char *str ) : str_(nullptr), doFree_(false)
			{
				_freeMem();
				_copyStr(str);
			} /* End of 'RobotWarException' class constructor */

			/* 'RobotWarException' class constructor
			 * ARGUMENTS:
			 *   - pointer to static string
			 *       const char *str;
			 *   - indicates that previous is static string
			 *       int;
			 */
			inline RobotWarException( const char *str, int ) : str_(str), doFree_(false) 
			{
			} /* End of 'RobotWarException' class constructor */

			/* 'RobotWarException' class constructor
			 * ARGUMENTS:
			 *   - reference to variable to copy
			 *       RobotWarException& ex;
			 */
			inline RobotWarException( const RobotWarException &ex ) : str_(nullptr), doFree_(false)
			{
				*this = ex;
			} /* End of 'RobotWarException' class constructor */

			/* Overload of assignment operator function
			 * ARGUMENTS:
			 *   - reference to variable to assign
			 *       const Exception &ex;
			 * RETURNS:
			 *   - self-reference
			 *       (RobotWarException &);
			 */
			inline RobotWarException & operator =( const RobotWarException &ex )
			{
				if (this != &ex)
				{
					_freeMem();
					if (ex.doFree_ == true)
						_copyStr(ex.str_);
					else
						str_ = ex.str_;
				}

			return *this;
		  } /* End of 'operator =' function */

			/* 'RobotWarException' class destructor
			 */
			inline virtual ~RobotWarException( void )
			{
				_freeMem();
			} /* End of 'RobotWarException' class destructor */

			/* Get exception string function
			 * ARGUMENTS: None.
			 * RETURNS:
			 *   - pointer to exception string
			 *       (const char *);
			 */
			inline const char * get( void ) const
			{
				return (str_ != nullptr ? str_ : "undefined exception");
			} /* End of 'get' function */
		}; /* End of 'RobotWarException' class */


		class UndefinedException : public RobotWarException
		{
		public:
			UndefinedException( const char *str ) : RobotWarException(str)
			{
			}

			UndefinedException( void ) : RobotWarException("undefined exception", 1)
			{
			}
		};

		class BadAlloc : public RobotWarException
		{
		public:
			BadAlloc( const char *str ) : RobotWarException(str)
			{
			}

			BadAlloc( void ) : RobotWarException("bad alloc", 1)
			{
			}
		};

		class RobotException : public RobotWarException
		{
		public:
			RobotException( const char *str ) : RobotWarException(str)
			{
			}

			RobotException( const char *str, int ) : RobotWarException(str, 1)
			{
			}

			RobotException( void ) : RobotWarException("robot exception", 1)
			{
			}
		};

		class LoadRobotException : public RobotException
		{
		public:
			LoadRobotException( const char *str ) : RobotException(str)
			{
			}

			LoadRobotException( void ) : RobotException("load robot exception", 1)
			{
			}
		};

		class NoValidMove : public RobotException
		{
		public:
			NoValidMove( const char *str ) : RobotException(str)
			{
			}

			NoValidMove( void ) : RobotException("no valid move", 1)
			{
			}
		};

		class ParametersContractError : public RobotWarException
		{
		public:
			ParametersContractError( const char *str ) : RobotWarException(str)
			{
			}

			ParametersContractError( void ) : RobotWarException("parameters contract error", 1)
			{
			}
		};

		class DllException : public RobotWarException
		{
		public:
			DllException( const char *str ) : RobotWarException(str)
			{
			}

			DllException( const char *str, int ) : RobotWarException(str, 1)
			{
			}

			DllException( void ) : RobotWarException("dll exception", 1)
			{
			}
		};

		class LoadDllException : public DllException
		{
		public:
			LoadDllException( const char *str ) : DllException(str)
			{
			}

			LoadDllException( void ) : DllException("load dll exception", 1)
			{
			}
		};

		class BindingProcDllException: public DllException
		{
		public:
			BindingProcDllException( const char *str ) : DllException(str)
			{
			}

			BindingProcDllException( void ) : DllException("binding procedure dll exception", 1)
			{
			}
		};
	}
}