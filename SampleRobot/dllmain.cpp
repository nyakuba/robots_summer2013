#include "SampleRobot.h"
#include "commondf.h"

using namespace RobotWar;

/** 
	@brief ���� ���� ����� �� ��������. ����� ��������� DllMain � �������������� �� dll ������� CreateRobot � DestroyRobot.
*/

HINSTANCE hInstanceDLL = 0;
//SampleRobot *myRobot = NULL;

BOOL WINAPI DllMain(HINSTANCE hInstanceDLL,DWORD fdwReason,
  void*)
{
	::hInstanceDLL = hInstanceDLL;
  return true;
}

/// @brief ������� DLL, ���������� ��� ������������� ������.
#pragma comment(linker,"/export:CreateRobot=?CreateRobot@@YA_NPAPAX@Z")
 __declspec(dllexport) bool  CreateRobot(void **MyRobot)
{
	*MyRobot = new SampleRobot;
	return true;
}

/// @brief ������� DLL, ���������� ��� ����������� ������.
#pragma comment(linker,"/export:DestroyRobot=?DestroyRobot@@YA_NPAPAX@Z")
 __declspec(dllexport) bool   DestroyRobot( void **MyRobot )
{
	delete dynamic_cast<SampleRobot *>(*(IRobot **)MyRobot);
	*MyRobot = nullptr;
	return true;
}